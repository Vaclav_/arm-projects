
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "stdio.h"

#include "driver/board.h"
#include "driver/fan_ctrl.h"
#include "driver/safety.h"
#include "driver/adc.h"
#include "DSP/PIcontroller.h"
#include "task/task.h"
//USB includes
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"


//Microcontroller: STM32F102CBT6

int main(void)
{
  DBGMCU_Config( DBGMCU_SLEEP, ENABLE);
    
  //init clock
  initBoard();
  
  initLEDs();

  initTim2();
  
  initFan1PWM();

/*
  //Fan 2 and safety init
  //Yet disabled becuase Fan2 is not yet implemented in the main loop
  
  initFan2PWM();
  
  int percent = 1;
  setFan1(percent);
  setFan2(percent);
  
  initBuzzer();
  buzzerOn();
  
  initTim3();
  initFan1Cnt();
  setCntSrc1();
  
  initADC();
  //End of Fan2 init
*/

////////USB init//////
  Set_System();
  Set_USBClock();
  USB_Interrupts_Config();
  USB_Init();
/////////////////////
  
  initDSP();
    
  //SYSTICK_TIMEBASE: clocks per second -> HCLK: 24MHz
  if (SysTick_Config( SYSTICK_TIMEBASE/100-1 ))
  { 
    //Error
    while (1);
  }
  
  while (1)
  {
    //Poll main task
    main_task();
    //Sleep, wait for interrupt
    __WFI();
    
  }
}
