
#ifndef _ADC_TEMP_H
#define _ADC_TEMP_H

  #include "stm32f10x.h"

  void initADC();
  uint16_t ADCget( uint8_t ch );

#endif