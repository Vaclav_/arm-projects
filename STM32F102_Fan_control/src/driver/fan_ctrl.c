
#include "driver/fan_ctrl.h"
#include "stm32f10x.h"
#include "driver/board.h"

//Fan1: cpu: tim2ch4
//Fan2: psu: tim2ch3
//Buzzer: tim2ch2
//Remap: partial remap2
void initTim2() {
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  
  //Init Timer2
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  
  //Time base configuration
  TIM_TimeBaseStructure.TIM_Period = FAN_CCR_MAX;
  TIM_TimeBaseStructure.TIM_Prescaler = TIM2_PRESCALER;
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  
  TIM_TimeBaseInit( TIM2, &TIM_TimeBaseStructure);
  
  TIM_ARRPreloadConfig(TIM2, ENABLE);
  
  TIM_Cmd(TIM2, ENABLE);
  
  GPIO_PinRemapConfig( GPIO_PartialRemap2_TIM2, ENABLE);
}
  
void initFan1PWM() {
  
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin = FAN1_CTRL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

  GPIO_Init( FAN1_CTRL_PORT, &GPIO_InitStructure);
  
  //RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);
  
  uint16_t CCR_Val = 1;
  
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  //PWM1 Mode configuration: Channel
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = CCR_Val;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC4Init(TIM2, &TIM_OCInitStructure);
}

void setFan1( unsigned int ctrl_percent ) {

 if( ctrl_percent > FAN_CCR_MAX)
    ctrl_percent = FAN_CCR_MAX;

  TIM_SetCompare4(TIM2, ctrl_percent);
}

//CNT:
//Fan1: TIM3_CH2        //Fan1 Pull: FAN1_PULL_PORT, FAN1_PULL_PIN
//Fan2: TIM3_CH1
//
void initTim3() {
  RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, ENABLE);
  
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  
  TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  
  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
}

void initFan1Cnt() {
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin = FAN1_CNT_PIN;
  //GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init( FAN1_CNT_PORT, &GPIO_InitStructure);
  
  GPIO_InitStructure.GPIO_Pin = FAN2_CNT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
  GPIO_Init( FAN2_CNT_PORT, &GPIO_InitStructure);
  //(no need to remap)
}

void setCntSrc1() {  
  //Set xternal clock source:
   TIM_TIxExternalClockConfig(TIM3, TIM_TIxExternalCLK1Source_TI2, TIM_ICPolarity_Rising, 0);
   
   TIM_SetCounter(TIM3,0);
   
   TIM_Cmd(TIM3, ENABLE);
}

unsigned int getFanCnt() {
  
  unsigned int timer_cnt = TIM_GetCounter(TIM3);
  TIM_SetCounter(TIM3,0);
  
  return timer_cnt;
}

void initFan2PWM() {
  
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin = FAN2_CTRL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;

  GPIO_Init( FAN2_CTRL_PORT, &GPIO_InitStructure);
  
  //RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
  
  uint16_t CCR_Val = 1;
  
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  //PWM1 Mode configuration: Channel
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = CCR_Val;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC3Init(TIM2, &TIM_OCInitStructure);
}

void setFan2( unsigned int ctrl_percent ) {

  if( ctrl_percent > FAN_CCR_MAX)
    ctrl_percent = FAN_CCR_MAX;

  TIM_SetCompare3(TIM2, ctrl_percent);
}