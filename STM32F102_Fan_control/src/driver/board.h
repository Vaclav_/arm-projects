#ifndef BOARD_H_
#define BOARD_H_

#include "stm32f10x.h"

#define GPIO4_PORT      GPIOB
#define GPIO3_PORT      GPIOB
#define GPIO2_PORT      GPIOB
#define FAN1_PULL_PORT  GPIOB
#define FAN1_CTRL_PORT  GPIOB
#define FAN2_CTRL_PORT  GPIOB
#define LED2_PORT       GPIOB
#define LED1_PORT       GPIOB
#define USB_DP_PORT     GPIOA
#define USB_DM_PORT     GPIOA
#define GPIO5_PORT      GPIOA
#define FAN1_CNT_PORT   GPIOA
#define FAN2_CNT_PORT   GPIOA
#define T2_PORT         GPIOA
#define T1_PORT         GPIOA
#define BUZZER_PORT     GPIOA

#define GPIO4_PIN      GPIO_Pin_15
#define GPIO3_PIN      GPIO_Pin_14
#define GPIO2_PIN      GPIO_Pin_13
#define FAN1_PULL_PIN  GPIO_Pin_12
#define FAN1_CTRL_PIN  GPIO_Pin_11
#define FAN2_CTRL_PIN  GPIO_Pin_10
#define LED2_PIN       GPIO_Pin_9
#define LED1_PIN       GPIO_Pin_8
#define USB_DP_PIN     GPIO_Pin_12
#define USB_DM_PIN     GPIO_Pin_11
#define GPIO5_PIN      GPIO_Pin_8
#define FAN1_CNT_PIN   GPIO_Pin_7
#define FAN2_CNT_PIN   GPIO_Pin_6
#define T2_PIN         GPIO_Pin_5
#define T1_PIN         GPIO_Pin_4
#define BUZZER_PIN     GPIO_Pin_1

//Runs from HCLK = 24MHz
//This div means 1 second
#define SYSTICK_TIMEBASE        24000000

void initBoard();
void clock_setup();

void initLEDs();

#endif