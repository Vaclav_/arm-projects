
#include "driver/adc.h"

//T1: ADC_IN4
//T2: ADC_IN5
//PCLK2: 3MHz
//Conversion: 14 cycles
//ADC clk div: 2

void initADC() {
  
  GPIO_InitTypeDef GPIO_InitStructure;
  
  //ADC_IN0
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  //Config ADC
  ADC_InitTypeDef  ADC_InitStructure;
  
  RCC_ADCCLKConfig(RCC_PCLK2_Div2);
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
  
  ADC_DeInit(ADC1);
  
  //ADC1 Configuration ---------------------------------------
  //ADC1 and ADC2 operate independently
  ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
  //Disable the scan conversion so we do one at a time
  ADC_InitStructure.ADC_ScanConvMode = DISABLE;
  //Don't do contimuous conversions - do them on demand
  ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
  //Start conversin by software, not an external trigger
  ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
  //Conversions are 12 bit - put them in the lower 12 bits of the result
  ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  //Say how many channels would be used by the sequencer
  ADC_InitStructure.ADC_NbrOfChannel = 1;
  
  //Now do the setup
  ADC_Init(ADC1, &ADC_InitStructure);
  //Enable ADC1
  ADC_Cmd(ADC1, ENABLE);

  //Enable ADC1 reset calibaration register
  ADC_ResetCalibration(ADC1);
  //Check the end of ADC1 reset calibration register
  while(ADC_GetResetCalibrationStatus(ADC1)) {}
  //Start ADC1 calibaration
  ADC_StartCalibration(ADC1);
  //Check the end of ADC1 calibration
  while(ADC_GetCalibrationStatus(ADC1)) {}
  
}

uint16_t ADCget( uint8_t ch ) {
  uint32_t i;
  uint32_t sum = 0;
  uint16_t val;
  
  ADC_RegularChannelConfig(ADC1, ch, 1, ADC_SampleTime_13Cycles5);

  for( i=0; i<1024; i++) {
    //Start the conversion
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
    //Wait until conversion completion
    while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET) {}
    //Get the conversion value
    sum += ADC_GetConversionValue(ADC1);
  }
  val = sum/1024;
  return val;
}