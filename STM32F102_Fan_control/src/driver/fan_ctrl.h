#ifndef _FAN_CTRL_H_
#define _FAN_CTRL_H_

//APB1CLK = HCLK = 24 MHz
//PWM freq: 1000
//PWM div: 1000
//base clock: 1 MHz

#define FAN_CCR_MAX 999
//Tim2: fan1, fan2, buzzer
#define TIM2_PRESCALER (24-1)

void initTim2();
void initFan1PWM();
void initFan2PWM();
void setFan1( unsigned int ctrl_percent );
void setFan2( unsigned int ctrl_percent );
void initTim3();
void initFan1Cnt();
void setCntSrc1();
unsigned int getFanCnt();

#endif