
#include "driver/board.h"
#include "driver/safety.h"

//buzzer: tim2ch2 (default mapping)

void initBuzzer() {
  //port should be enabled already
  GPIO_InitTypeDef GPIO_InitStructure;

  GPIO_InitStructure.GPIO_Pin = BUZZER_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;

  GPIO_Init( BUZZER_PORT, &GPIO_InitStructure);
  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
  TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);
}

//1000 Hz 50% PWM signal
void buzzerOn() {
  uint16_t CCR_Val = 499;
  
  TIM_OCInitTypeDef  TIM_OCInitStructure;
  //PWM1 Mode configuration: Channel2
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = CCR_Val;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC2Init(TIM2, &TIM_OCInitStructure);
}

void buzzerOff() {
  uint16_t CCR_Val = 0;

  TIM_OCInitTypeDef  TIM_OCInitStructure;
  //PWM1 Mode configuration: Channel2
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Disable;
  TIM_OCInitStructure.TIM_Pulse = CCR_Val;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC2Init(TIM2, &TIM_OCInitStructure);
}