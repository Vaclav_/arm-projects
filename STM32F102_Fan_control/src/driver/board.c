#include "stm32f10x.h"
#include "driver/board.h"

void initBoard() {
  clock_setup();
  
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  RCC_APB2PeriphClockCmd( RCC_APB2Periph_AFIO, ENABLE);
  
  //Must be pulled low to ensure the FET's gate isn't floating
  GPIO_InitTypeDef gpio_struct;
  gpio_struct.GPIO_Pin = FAN1_PULL_PIN;
  gpio_struct.GPIO_Speed = GPIO_Speed_2MHz;
  gpio_struct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init( FAN1_PULL_PORT, &gpio_struct);
  GPIO_ResetBits( FAN1_PULL_PORT, FAN1_PULL_PIN);
}

void initLEDs() {
  
  GPIO_InitTypeDef gpio_struct;	 
  
  gpio_struct.GPIO_Pin = LED1_PIN;
  gpio_struct.GPIO_Speed = GPIO_Speed_2MHz;
  gpio_struct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init( LED1_PORT, &gpio_struct);
  
  gpio_struct.GPIO_Pin = LED2_PIN;
  gpio_struct.GPIO_Speed = GPIO_Speed_2MHz;
  gpio_struct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init( LED2_PORT, &gpio_struct);
  
}

//Clock:
//External crystal: 4 MHz
//PLL: 48Mhz = 12 * 4
//USB div: 1
//SYSCLK = PLLCLK
//HCLK = SYSCLK/2 = 24 MHz
//APB1CLK = HCLK = 24 MHz
//APB2CLK = HCLK/8 = 3 MHz
//ADCCLK:
void clock_setup() {
  __IO uint32_t StartUpCounter = 0, HSEStatus = 0;
  
  /* SYSCLK, HCLK, PCLK2 and PCLK1 configuration ---------------------------*/    
  /* Enable HSE */    
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);
 
  /* Wait till HSE is ready and if Time out is reached exit */
  do
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;  
  } while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

  if ((RCC->CR & RCC_CR_HSERDY) != RESET)
  {
    HSEStatus = (uint32_t)0x01;
  }
  else
  {
    HSEStatus = (uint32_t)0x00;
  }  

  if (HSEStatus == (uint32_t)0x01)
  {

    /* Enable Prefetch Buffer */
    FLASH->ACR |= FLASH_ACR_PRFTBE;

    /* Flash 0 wait state */
    FLASH->ACR &= (uint32_t)((uint32_t)~FLASH_ACR_LATENCY);
    FLASH->ACR |= (uint32_t)FLASH_ACR_LATENCY_0;    
    
    // HCLK = SYSCLK/2 = 24 MHz
    RCC_HCLKConfig( RCC_SYSCLK_Div2 );
      
    //PCLK2 = HCLK/8 = 3 MHz
    RCC_PCLK2Config( RCC_HCLK_Div8 );
    
    // PCLK1 = HCLK = 24 MHz
    RCC_PCLK1Config( RCC_HCLK_Div1 );
    
    // PLL: HSE * 12 = 4 * 12 = 48 MHz
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMULL));
    RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_HSE | RCC_CFGR_PLLMULL12);

    // Enable PLL
    RCC->CR |= RCC_CR_PLLON;

    // Wait till PLL is ready
    while((RCC->CR & RCC_CR_PLLRDY) == 0)
    {
    }

    /* Select PLL as system clock source */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
    RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;    

    /* Wait till PLL is used as system clock source */
    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)0x08)
    {
    }
  }
  else
  { /* If HSE fails to start-up, the application will have wrong clock 
         configuration. User can add here some code to deal with this error */
  }
}