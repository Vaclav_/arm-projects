
#include "filter.h"

void initFilter( filter_data* fp, uint8_t val ) {
  if( fp == 0 )
    return;
  int i;
  for( i=0; i < FILTER_TAP_NUM; i++) {
    fp->values[i] = val;
  }
}

uint8_t procFilter( filter_data* fp, uint8_t new_val) {
  if( fp == 0 )
    return 0;
  int i;
  
  fp->values[fp->cur_p] = new_val;
  fp->cur_p++;
  if( fp->cur_p >= FILTER_TAP_NUM )
    fp->cur_p = 0;
  
  uint32_t sum = 0;
  uint32_t val;
  for( i=0; i<FILTER_TAP_NUM; i++) {
    val = fp->values[i];
    val <<= 16;
    sum += val;
  }
  val = FILTER_TAP_NUM;
  val <<= 16;
  sum /= val;
  
  return sum;
}