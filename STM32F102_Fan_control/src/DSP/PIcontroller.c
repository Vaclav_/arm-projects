
#include "PIcontroller.h"

void initPI( pi_controller_struct* pi_struct ) {
    if( !pi_struct ) return;
    pi_struct->P_REF = 35;
    pi_struct->I_REF = 60;
    pi_struct->P_GAIN = 2<<16;
    pi_struct->I_GAIN = (uint32_t)(0.015f*(1<<16));
    pi_struct->INTEGRAL_SAT = 50<<16;
    pi_struct->i_out = 0;
    pi_struct->p_out = 0;
}


//In: ym: measured temp 0-120 Celsius
//Out: u: fan control: 0-100 %
uint8_t procPI( uint8_t y_measured, pi_controller_struct* pi_struct ) {

    if( !pi_struct )
        return 0xFF;

    if( y_measured > 150 )
        y_measured = 150;

    uint32_t i_out = pi_struct->i_out;
    uint32_t p_out = pi_struct->p_out;

    //e = r - ym
    //e>0 -> turn fan on (u>0)
    //e<0 -> no action, u=0
    uint32_t p_error;
    //uint32_t p_out;
    if( y_measured <= pi_struct->P_REF )
        p_out = 0;
    else {
        p_error = y_measured - pi_struct->P_REF;       //no underflow
        p_error <<= 16;                     //convert to 16:16
        uint64_t p_out64 = (uint64_t)p_error*pi_struct->P_GAIN;
        p_out = p_out64 >> 16;               //back to 16:16
        if( p_out > 100<<16 )
            p_out = 100<<16;
    }

    int16_t i_error;
    uint32_t i_error_un;
    uint64_t i_change;
    i_error = (int16_t)y_measured - pi_struct->I_REF;
    if( i_error >= 0 ) {
        i_error_un = i_error;       //positive
        i_error_un <<= 16;
        //err*dt*K
        i_change = (uint64_t)i_error_un*pi_struct->I_GAIN;
        i_change >>= 16;
        if( i_change > 100<<16)
            i_change = 100<<16;
        i_out += (uint32_t)i_change;
        //saturate
        if( i_out > pi_struct->INTEGRAL_SAT )
            i_out = pi_struct->INTEGRAL_SAT;
    } else {
        i_error_un = -i_error;      //-error = positive
        i_error_un <<= 16;
        //err*dt*K
        i_change = (uint64_t)i_error_un*pi_struct->I_GAIN;
        i_change >>= 16;
        if( i_change > 100<<16)
            i_change = 100<<16;
        if( (uint32_t)i_change >= i_out )
            i_out = 0;
        else
            i_out -= (uint32_t)i_change;
    }

    uint32_t pi_out = p_out + i_out;
    pi_out >>= 16;
    if( pi_out > 100 )
        pi_out = 100;

    pi_struct->i_out = i_out;
    pi_struct->p_out = p_out;

    return pi_out;
}
//Form controller's final output
//which is the input of the actuator
//physically it's the PWM cnt of the signal
static uint8_t output1_on = 0;  //used for implementing hysteresis

uint16_t form_output1(uint8_t u ) {
  
  if( output1_on ) {
    if( u < 2) {
      output1_on = 0;   //turn off
      return 0; 
    }
    if( u < 10)
      return 675;       //665 + 10
  } else {
    if( u > 9) {
      output1_on = 1; //turn on
      return ( 665 + u );
    } else {
      return 0;
    }
  }
  
  //if( u < 5)
  //  return 0;
  if( u <= 50 ) {
    return ( 665 + u );
  }
  if( u > 100 )
    u = 100;
  // 50 < u <= 100
  return ( 715 + (u-50)*2 );
}
