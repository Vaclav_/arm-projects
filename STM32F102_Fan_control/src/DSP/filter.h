
#ifndef _FILTER_H_
#define _FILTER_H_

  #include <inttypes.h>

  #define FILTER_TAP_NUM  10

  typedef struct {
    uint8_t values[FILTER_TAP_NUM];
    uint8_t cur_p;
  } filter_data;

  void initFilter( filter_data* fp, uint8_t val );
  uint8_t procFilter( filter_data* fp, uint8_t new_val);

#endif