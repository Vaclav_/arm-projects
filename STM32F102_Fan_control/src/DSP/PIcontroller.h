#ifndef PICONTROLLER_H_INCLUDED
#define PICONTROLLER_H_INCLUDED

#include <inttypes.h>

typedef struct {
    uint8_t P_REF;      //Reference value from 20 to 100
    uint8_t I_REF;      //May be different for P and I parts
    uint32_t P_GAIN;    //Fixed point 16:16 (all below)
    uint32_t I_GAIN;
    uint32_t INTEGRAL_SAT;  //Integrator's max value
    uint32_t i_out;     //Integrator's output value
    uint32_t p_out;     //Proportional unit's output, used only for debugging
} pi_controller_struct;

void initPI( pi_controller_struct* pi_struct );
uint8_t procPI( uint8_t y_measured, pi_controller_struct* pi_struct );
//Form output of fan1, aka CPU fan
uint16_t form_output1( uint8_t u );

#endif // PICONTROLLER_H_INCLUDED
