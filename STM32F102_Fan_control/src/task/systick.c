
#include "stm32f10x.h"
#include "driver/board.h"
#include <inttypes.h>




#define SYSTICK_MS      10

volatile uint16_t time_recv;
volatile uint16_t time_got_temp;
volatile uint16_t time_check_cnt;

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
//Systick: every 10ms
void SysTick_Handler(void)
{
  time_recv += SYSTICK_MS;
  time_got_temp += SYSTICK_MS;
  time_check_cnt += SYSTICK_MS;
}

//Systick: every 10 ms
//task1: check for incoming data: 10ms --process PID, set actuator, send data: 500ms
//task2: check CNT error -> buzzer - 500 ms / other phase
