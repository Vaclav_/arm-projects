
#include "communication.h"

//USB includes
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_desc.h"
#include "usb_pwr.h"


//USB variables
extern __IO uint8_t Receive_Buffer[64];
extern __IO  uint32_t Receive_length ;
extern __IO  uint32_t length ;
uint8_t Send_Buffer[64];
uint32_t packet_sent=1;
uint32_t packet_receive=1;


//check_recv fcn private variables
//number of bytes processed in buffer
static uint32_t processed_n = 0;
//bytes of data to be received, 0 is for command
static uint8_t data_n = 0;

//Receives one command and it's data (parameters) at a time
//Must poll it until it returns RECV_NOTHING or an error
e_recv_state check_recv( recv_data_t* data_struct ) {
  
  if( data_struct == 0 )
    return RECV_ERROR;
  
  //Check if connected
  if (bDeviceState == CONFIGURED)
  {
    //If all data in buffer processed - request new data
    if( processed_n >= Receive_length ) {
      Receive_length = 0;
      CDC_Receive_DATA();
      processed_n = 0;
    }
    //Check to see if we have data yet
    if (Receive_length  != 0)
    {
      do {
        //if buffer is processed but information isn't complete
        //return NOTHING, and receive the rest in next cycle
        if( processed_n >= Receive_length ) {
          processed_n = 0;
          Receive_length = 0;
          return RECV_NOTHING;
        }
        
        if( data_n == 0 ) {    //next byte is a command
          data_struct->cmd = Receive_Buffer[processed_n];
          data_n = 1;     //TODO: set based on command type
          data_struct->data = 0;
          processed_n++;
        } else {        //receive n byte of data for the command
          data_n--;
          //receives MSB first
          uint8_t* data_addr = (uint8_t*)&(data_struct->data);
          *(data_addr+data_n) = Receive_Buffer[processed_n];
          processed_n++;
        }
      } while( data_n);
         
      return RECV_OK;
      
    } else
      return RECV_NOTHING;
    
  } else
    return RECV_UNCONNECTED;
  
}

void usb_send( uint8_t* data_p, uint8_t data_size) {
  if( data_p == 0)
    return;
  if(bDeviceState != CONFIGURED)
    return;
  
  while(packet_sent != 1) {}       //wait for previous packet to be sent
  CDC_Send_DATA ( data_p, data_size);
}