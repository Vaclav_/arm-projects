
#ifndef _MY_COMM_H_
#define _MY_COMM_H_

#include <inttypes.h>

typedef enum {
  RECV_NOTHING,
  RECV_OK,
  RECV_ERROR,
  RECV_UNCONNECTED
} e_recv_state;

typedef enum {
  CMD_NONE = 0,
  CMD_CPU_TEMP = 't',
  CMD_SET_P_REF1 = 'p',
  CMD_SET_I_REF1 = 'i',
  CMD_SET_P_GAIN1,
  CMD_SET_I_GAIN1,
  CMD_RESET
} e_pc_commands;

//send:
//1 command

typedef struct {
  uint8_t cmd;
  uint32_t data;
} recv_data_t;

e_recv_state check_recv( recv_data_t* data_struct );

void usb_send( uint8_t* data_p, uint8_t data_size);

#endif