
#include "communication.h"
#include "DSP/PIcontroller.h"
#include "driver/fan_ctrl.h"
#include "DSP/filter.h"

#define COMM_TIMEOUT    600
#define CHECK_CNT       250
#define RECV_TIME       50

//Timing
extern volatile uint16_t time_recv;
extern volatile uint16_t time_got_temp; //received temperature
extern volatile uint16_t time_check_cnt;

static uint8_t last_cpu_temp = 70;
static uint8_t got_temp = 0;    //true/false
static uint8_t filter_init_ok = 0;
static filter_data output_filter1;

static recv_data_t recv_data;

static pi_controller_struct CPU_fan_controller;

void initDSP() {
  initPI(&CPU_fan_controller);
}

void proc_cmd( recv_data_t* data_struct ) {
  if( data_struct == 0 )
    return;
  
  if( data_struct->cmd == CMD_CPU_TEMP ) {
    last_cpu_temp = (uint8_t)data_struct->data;
    time_got_temp = 0;
    got_temp = 1;
  }
  
  //echo
  //usb_send( &data_struct->cmd, 1);
  //usb_send( (uint8_t*)&data_struct->data, 1);
}

void main_task() {
  
  //Retreive all packets from buffer
  if( time_recv >= RECV_TIME ) {
    time_recv = 0;
    e_recv_state recv_ret;
    do {
      recv_ret = check_recv(&recv_data);
      if( recv_ret == RECV_OK )
        proc_cmd(&recv_data);
    } while( recv_ret == RECV_OK );
  }
  
  //Check if got temperature from PC or not
  //Do PI if got it, or at timeout
  uint8_t pi_output;
  if( got_temp || (time_got_temp >= COMM_TIMEOUT) ) {
    //process PI controller
    pi_output = procPI( last_cpu_temp, &CPU_fan_controller);
    
    if( got_temp) {
      if( !filter_init_ok ) {
        filter_init_ok = 1;
        initFilter( &output_filter1, pi_output);
      }
    }
    
    if( filter_init_ok ) {
      pi_output = procFilter( &output_filter1, pi_output);
    }

    uint16_t act_signal;
    //form output signal
    act_signal = form_output1( pi_output );

    //apply signal
    setFan1(act_signal);
    //Send back the value for debug
    uint8_t cmd = 'a';
    if( got_temp) {
      usb_send( &cmd, 1);
      usb_send( &pi_output, 1);
    }
    //printf( "act: %i\n", act_signal);
    time_got_temp = 0;
    got_temp = 0;
  }
  
}