
#include "my_i2c.h"

#define I2C_WRITE 0
#define I2C_READ 1

//0b1010000
#define EEPROM_CTRL_CODE 0x50

uint8_t I2CInit() {
  
  ROM_SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOA );
  GPIOPinTypeI2CSCL( GPIO_PORTA_BASE, GPIO_PIN_6 );
  GPIOPinTypeI2C( GPIO_PORTA_BASE, GPIO_PIN_7 );  //SDA
  GPIOPinConfigure( GPIO_PA6_I2C1SCL );
  GPIOPinConfigure( GPIO_PA7_I2C1SDA );
  
  ROM_SysCtlPeripheralEnable( SYSCTL_PERIPH_I2C1);
  
  //0 -> 100kHz 
  //1 -> 400kHz
  ROM_I2CMasterInitExpClk( I2C1_MASTER_BASE, SysCtlClockGet(), 1);
  
  return 0;
}


// I2C Write Byte
// return 0: Write succeded
//        1: Parameter is NULL
//        2: I2C error
//        3: I2C error
//        4: I2C error
//        5: I2C error
uint8_t I2CWriteByte(I2C_packet *writebyte)
{
  //NULL pointer
  if( writebyte == 0 )
    return 1;
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}

  //Calculate the i2c address to be sent
  uint8_t i2c_addr = EEPROM_CTRL_CODE | (writebyte->CS_bits & 0x07);

  //Set slave address
  ROM_I2CMasterSlaveAddrSet( I2C1_MASTER_BASE, i2c_addr, I2C_WRITE);
  
  //High byte of the address of the data to be witten
  int8_t data_addr_H = (uint8_t)(writebyte->address >> 8);
  
  //Put data into data register
  ROM_I2CMasterDataPut( I2C1_MASTER_BASE, data_addr_H );
  
  //Start transmission
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_START);
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
  
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 2;
  }
  
  //Low byte of the address of the data to be witten
  int8_t data_addr_L = (uint8_t)(writebyte->address);
  
  //Put data into data register
  ROM_I2CMasterDataPut(I2C1_MASTER_BASE, data_addr_L );
  
  //Start transmission
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);

  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
 
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 3;
  }
  
  //Put data into data register
  ROM_I2CMasterDataPut(I2C1_MASTER_BASE, writebyte->data );
  
  //Start transmission
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);

  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
 
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 4;
  }
  
  //Finish sequence
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
  
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 5;
  }
  
  return 0;
}

//I2C Read Byte
//returns:
//      0       Success
//      1       Pointer is NULL
//      2       I2c error
//      3       I2c error
//      4       I2c error
//      5       I2c error
uint8_t I2CReadByte(I2C_packet *readbyte) {
  
  if( readbyte == 0 )
    return 1;
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}

  //First we write the address of the data to be read
  //Calculate the i2c address to be sent
  uint8_t i2c_addr = EEPROM_CTRL_CODE | (readbyte->CS_bits & 0x07);

  //Set slave address
  ROM_I2CMasterSlaveAddrSet( I2C1_MASTER_BASE, i2c_addr, I2C_WRITE);
  
  //High byte of the address of the data to be read
  int8_t data_addr_H = (uint8_t)(readbyte->address >> 8);
  
  //Put data into data register
  ROM_I2CMasterDataPut( I2C1_MASTER_BASE, data_addr_H );
  
  //Start transmission
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_START);
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
  
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 2;
  }
  
  //Low byte of the address of the data to be read
  int8_t data_addr_L = (uint8_t)(readbyte->address);
  
  //Put data into data register
  ROM_I2CMasterDataPut(I2C1_MASTER_BASE, data_addr_L );
  
  //Start transmission
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);

  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
 
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 3;
  }
  
  //Calculate the i2c address to be sent
  i2c_addr = EEPROM_CTRL_CODE | (readbyte->CS_bits & 0x07);
  I2CMasterSlaveAddrSet( I2C1_MASTER_BASE, i2c_addr, I2C_READ);
  
  I2CMasterControl( I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_RECEIVE_START);
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
  
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    readbyte->data = 0xff;
    return 4;
  }
  
  readbyte->data = I2CMasterDataGet(I2C1_MASTER_BASE);
  
  //Finish sequence
  ROM_I2CMasterControl(I2C1_MASTER_BASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);
  
  //Wait until i2c bus is busy
  while(ROM_I2CMasterBusy(I2C1_MASTER_BASE)) {}
  
  if(ROM_I2CMasterErr(I2C1_MASTER_BASE) != I2C_MASTER_ERR_NONE) {
    //Error
    return 5;
  }
  
  return 0;
}