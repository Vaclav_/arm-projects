//
// Author: Patrik T�ri
// Last modified: 5.12.2013
// Program:
//      EEPROM test with I2C communication.
//      Uses virtual serial port as user interface. (115200, 8, 0, 1)
//      Testing IC: 24LC128 I/P
//        Datasheet: http://ww1.microchip.com/downloads/en/DeviceDoc/21191s.pdf
//      I2C: 400kHz, 2k pull-up resistor
//      The rest pins are unconnected.
//

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "inc/hw_timer.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"

#include "my_i2c.h"

#define PIN_BTN_RIGHT      GPIO_PIN_0
#define PIN_BTN_LEFT       GPIO_PIN_4
#define PIN_LED_RED        GPIO_PIN_1
#define PIN_LED_BLUE       GPIO_PIN_2
#define PIN_LED_GREEN      GPIO_PIN_3

void CheckStartupButton();
void Error( const char* err_msg, int i );
void BtnRightInit();
void VirtualUART0Init();
uint16_t scanAddress();
uint8_t scanData();

int main()
{
  CheckStartupButton();
  
  //80MHz
  SysCtlClockSet(
    SYSCTL_SYSDIV_2_5 |
    SYSCTL_USE_PLL |
    SYSCTL_OSC_MAIN |
    SYSCTL_XTAL_16MHZ
  );
  
  VirtualUART0Init();
  UARTprintf("Starting EEPROM test...\n");
  
  BtnRightInit();
  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  GPIOPinTypeGPIOOutput( GPIO_PORTF_BASE, PIN_LED_RED);
  GPIOPinTypeGPIOOutput( GPIO_PORTF_BASE, PIN_LED_GREEN);
  I2CInit();
  
  GPIOPinWrite( GPIO_PORTF_BASE, PIN_LED_RED, 0);
  GPIOPinWrite( GPIO_PORTF_BASE, PIN_LED_GREEN, PIN_LED_GREEN);
  
  I2C_packet write_packet;
  I2C_packet read_packet;
  read_packet.CS_bits = 0;
  write_packet.CS_bits = 0;

  SysCtlDelay( SysCtlClockGet()/12 );

  uint8_t mode_select;
  int err_code;
  
  while(1) {
    UARTprintf("Select Write Mode\n1\tByte R/W\n2\tPage R/W x\n");
    mode_select = UARTgetc();
    if( mode_select == '1' ) {
      while( mode_select != '3' ) {
        UARTprintf("Specify operation\n1\tWrite\n2\tRead\n3\tReturn\n");
        mode_select = UARTgetc();
        if( mode_select == '1' ) {
          write_packet.address = scanAddress();
          write_packet.data = scanData();
          UARTprintf("Writing data...");
          
          err_code = I2CWriteByte( &write_packet );
          if( err_code )
            Error("I2C failure", err_code);
          else {
            UARTprintf("Written data succefully.\n\n");
          }
          
        } else if( mode_select == '2') {
          read_packet.address = scanAddress();
          
          UARTprintf("Reading data...");
          
          err_code = I2CReadByte( &read_packet );
          if( err_code )
            Error("I2C failure", err_code);
          else {
            UARTprintf("Read data: 0x%x  (%i).\n\n", read_packet.data, read_packet.data);
          }
          
        } else if( mode_select != '3')
          UARTprintf("Invalid command.\n");
      }
    } else if( mode_select == '2' ) {
      UARTprintf("Not yet implemented.\n\n");
    } else
      UARTprintf("Invalid command.\n");
  }
  

}

void Error( const char* err_msg, int i ) {
  GPIOPinWrite( GPIO_PORTF_BASE, PIN_LED_GREEN, 0);
  GPIOPinWrite( GPIO_PORTF_BASE, PIN_LED_RED, PIN_LED_RED);
  UARTprintf("\nError: %s, error code: %i\n", err_msg, i);
  while(1) {
  }
}

//Keeps waiting if the left button is pressed
void CheckStartupButton() {
  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  GPIODirModeSet( GPIO_PORTF_BASE, PIN_BTN_LEFT, GPIO_DIR_MODE_IN );
  GPIOPadConfigSet( GPIO_PORTF_BASE, PIN_BTN_LEFT, 
                   GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU );
  
  while( !GPIOPinRead( GPIO_PORTF_BASE, PIN_BTN_LEFT ) ) {}
}

void BtnRightInit() {
  //Right button: PORTF Pin 0
  //Must unlock before configuring
  HWREG( GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY_DD;
  HWREG( GPIO_PORTF_BASE + GPIO_O_CR) = 0x01;   //Commit register
  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  GPIODirModeSet( GPIO_PORTF_BASE, PIN_BTN_RIGHT, GPIO_DIR_MODE_IN );
  GPIOPadConfigSet( GPIO_PORTF_BASE, PIN_BTN_RIGHT, 
                   GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU );
  HWREG( GPIO_PORTF_BASE + GPIO_O_LOCK) = 0;
}

void VirtualUART0Init() {
  //Initialize UART0
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  GPIOPinConfigure(GPIO_PA0_U0RX);
  GPIOPinConfigure(GPIO_PA1_U0TX);
  GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
  //0 for UART0
  //115200 bit/s
  //8 data bit - No parity - 1 Stop bit
  UARTStdioConfig( 0, 115200, SysCtlClockGet());
}
