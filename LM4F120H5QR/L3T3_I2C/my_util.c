#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "inc/hw_timer.h"
#include "driverlib/timer.h"
#include "utils/uartstdio.h"
#include "inttypes.h"


uint16_t addrToUint16( uint8_t* ch ) {
  uint16_t ret_addr = 0;
  int i, k;
  uint16_t num;
  for( i=0; i<4; i++) {
    if( ch[3-i] >= '0' && ch[3-i] <= '9' ) {
      num = ch[3-i]-'0';
      for( k=0; k<i; k++ )
        num *= 16;
      ret_addr += num;
    } else {
      num = ch[3-i]-'a'+10;
      for( k=0; k<i; k++ )
        num *= 16;
      ret_addr += num;
    }
  }
  return ret_addr;
}

void ToLower( uint8_t* c ) {
  if(c!=0) {
    if( *c >= 'A' && *c <= 'Z' )
      *c += 'a'-'A';
  }
}

uint16_t scanAddress() {
  
  uint8_t addr_input[4];
  uint16_t ret_addr=0;
  
  int addr_ok = 0;
  while( !addr_ok ) {
    UARTprintf("Enter address: 0x");
    int i;
    for( i=0; i<4; i++) {
      addr_input[i] = UARTgetc();
      ToLower( &addr_input[i] );
      UARTprintf( "%c", addr_input[i]);
    }
    UARTprintf("\n");
   
    addr_ok = 1;
    for( i=0; i<4; i++) {
      if( ! ( (addr_input[i]  >= '0' && addr_input[i] <= '9') ||
          ( addr_input[i] >= 'a' && addr_input[i] <= 'f' ) )
        ) {
          addr_ok = 0;
          break;
        } // else address is ok
    }
    
      
    if( !addr_ok ) {
      UARTprintf("Invalid address.\n");
      continue;
    }
    
    ret_addr = addrToUint16(addr_input);
    
    if( ret_addr > 0x3FFF ) {
      UARTprintf("Invalid address, address is out of range (128kB).\n");
      addr_ok = 0;
      continue;
    }
    
    UARTprintf("Address entered: 0x%x  (%i)\n", ret_addr, ret_addr);
    
   }
  
  return ret_addr;
}

uint8_t dataToUint8( uint8_t* ch ) {
  uint16_t ret_addr = 0;
  int i, k;
  uint16_t num;
  for( i=0; i<2; i++) {
    if( ch[1-i] >= '0' && ch[1-i] <= '9' ) {
      num = ch[1-i]-'0';
      for( k=0; k<i; k++ )
        num *= 16;
      ret_addr += num;
    } else {
      num = ch[1-i]-'a'+10;
      for( k=0; k<i; k++ )
        num *= 16;
      ret_addr += num;
    }
  }
  return ret_addr;
}

uint8_t scanData() {
  
  uint8_t data_input[2];
  uint8_t ret_data=0;
  
  int data_ok = 0;
  while( !data_ok ) {
    UARTprintf("Enter data: 0x");
    int i;
    for( i=0; i<2; i++) {
      data_input[i] = UARTgetc();
      ToLower( &data_input[i] );
      UARTprintf( "%c", data_input[i]);
    }
    UARTprintf("\n");
    
    data_ok = 1;
    for( i=0; i<2; i++) {
      if( ! ( (data_input[i]  >= '0' && data_input[i] <= '9') ||
          ( data_input[i] >= 'a' && data_input[i] <= 'f' ) )
        ) {
          data_ok = 0;
          break;
        } // else data is ok
    }
    
    if( !data_ok ) {
      UARTprintf("Invalid data.\n");
      continue;
    }
    
    ret_data = dataToUint8(data_input);
    
    UARTprintf("Data entered: 0x%x  (%i)\n", ret_data, ret_data);
    
  }
  return ret_data;
}