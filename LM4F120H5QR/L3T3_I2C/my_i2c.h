
#ifndef MY_I2C_H_
#define MY_I2C_H_

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"
#include "inc/hw_timer.h"
#include "driverlib/timer.h"
#include "driverlib/rom.h"

#include "inttypes.h"

#include "driverlib/i2c.h"

typedef struct {
  uint8_t CS_bits;      //A0..A2
  uint16_t address;
  uint8_t data;
} I2C_packet;

uint8_t I2CInit();
uint8_t I2CWriteByte(I2C_packet *writebyte);
uint8_t I2CReadByte(I2C_packet *writebyte);

#endif