//
// Author: Patrik T�ri
// Last modified: 5. 12. 2013
// Program: brightness control of the red LED with PWM.
//      Hold Left button: increase brightness
//      Hold Right button: decrease brightness
//
//

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "inc/hw_timer.h"
#include "driverlib/timer.h"

#define GPIO_PIN_BTN_RIGHT      GPIO_PIN_0
#define GPIO_PIN_BTN_LEFT       GPIO_PIN_4
#define GPIO_PIN_LED_RED        GPIO_PIN_1
#define GPIO_PIN_LED_BLUE       GPIO_PIN_2
#define GPIO_PIN_LED_GREEN      GPIO_PIN_3

void CheckStartupButton();
void BtnRightInit();

int main()
{ 
  CheckStartupButton();
  
  SysCtlClockSet(
    SYSCTL_SYSDIV_4 |
    SYSCTL_USE_OSC |
    SYSCTL_OSC_MAIN |
    SYSCTL_XTAL_16MHZ
  );
  
  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );

  BtnRightInit();

  //Timer CCP mode:
  //In normal operation, counter counts down from �load value� 
  //and PWM signal stays high until the counter reaches �match value�,
  //at which point PWM signal goes down and stays there until timer reaches zero,
  //at which point the counter is reset to �load value�, PWM signal goes back up,
  //and downcount continues.
  
  //System clock: 4MHz
  //PWM period: 100Hz
  //Pulse/ON time: 1000 resolution
  SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
  TimerConfigure(TIMER0_BASE, TIMER_CFG_SPLIT_PAIR|TIMER_CFG_B_PWM);
  
  //Set up PWM with timer Capture Compare PWM (CCP) mode
  //100Hz period
  unsigned short ulPeriod = SysCtlClockGet() / 100;     //=40000, 16bit value
  unsigned short dutyCycle = 500;                       //0.1%, 0-1000
  unsigned short pulse = ulPeriod/1000*dutyCycle;
  TimerLoadSet(TIMER0_BASE, TIMER_B, ulPeriod - 1);
  TimerMatchSet(TIMER0_BASE, TIMER_B, pulse);
  TimerEnable(TIMER0_BASE, TIMER_B);
  
  //Configure PIN to PWM functionality
  GPIOPinConfigure(GPIO_PF1_T0CCP1);                    //mux T0CCP1 to PF1
  GPIOPinTypeTimer(GPIO_PORTF_BASE, GPIO_PIN_1);        //set up the pin
  
  //TIMER_TBMR_TBMRSU : Make the changes to match registers take effect on 
  //                    the start of the next PWM period, not immediately
  //                    -> prevents occasional flickering when the match value
  //                    is increased
  //TIMER_TBMR_TBPLO : Make corresponding CCP pin go HIGH n the beginning of the PWM period
  //TIMER_TBMR_TBILD : Make the changes to reload (period) registers take 
  //                   effect on the start of the next PWM period, not immediately 
  //For timer A replace TB for TA in every register/flag name
  HWREG(TIMER0_BASE + TIMER_O_TBMR) |= TIMER_TBMR_TBMRSU | TIMER_TBMR_TBPLO | TIMER_TBMR_TBILD;
  
  while(1) {
    if( !GPIOPinRead( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT ) ) {
      if( dutyCycle > 0 ) {
        dutyCycle--;
        pulse = ulPeriod/1000*dutyCycle;
        TimerMatchSet(TIMER0_BASE, TIMER_B, pulse);
      }
    } else if( !GPIOPinRead( GPIO_PORTF_BASE, GPIO_PIN_BTN_RIGHT ) ) {
      if( dutyCycle < 1000 ) {
        dutyCycle++;
        //-2 is needed because if we get to close to the loaded value
        //the port will be stuck at high level
        pulse = ulPeriod/1000*dutyCycle - 2;
        TimerMatchSet(TIMER0_BASE, TIMER_B, pulse);
      }
    }
    //change rate: 400/second
    //->Delay: 4000000/400/3 = 3333
    SysCtlDelay(3333);
  }
  
}

//Keeps waiting if the left button is pressed
void CheckStartupButton() {

  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  GPIODirModeSet( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT, GPIO_DIR_MODE_IN );
  GPIOPadConfigSet( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT, 
                   GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU );

  while( !GPIOPinRead( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT ) ) {}
}

void BtnRightInit() {
  //Right button: PORTF Pin 0
  //Must unlock before configuring
  HWREG( GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY_DD;
  HWREG( GPIO_PORTF_BASE + GPIO_O_CR) = 0x01;   //Commit register
  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  GPIODirModeSet( GPIO_PORTF_BASE, GPIO_PIN_BTN_RIGHT, GPIO_DIR_MODE_IN );
  GPIOPadConfigSet( GPIO_PORTF_BASE, GPIO_PIN_BTN_RIGHT, 
                   GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU );
  HWREG( GPIO_PORTF_BASE + GPIO_O_LOCK) = 0;
}