//
// Author: Patrik T�ri
// Last modified: 5. 12. 2013
// Program: Virtual serial port, 115200kbit/s, no paraity, 1 stop, 8 bit
//      Echoes everything unchanged on uart0.
//

#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "utils/uartstdio.h"

#define GPIO_PIN_BTN_RIGHT      GPIO_PIN_0
#define GPIO_PIN_BTN_LEFT       GPIO_PIN_4
#define GPIO_PIN_LED_RED        GPIO_PIN_1
#define GPIO_PIN_LED_BLUE       GPIO_PIN_2
#define GPIO_PIN_LED_GREEN      GPIO_PIN_3

void CheckStartupButton();

//UART Interrupt indicator flags
unsigned long g_ulFlags;

int main()
{ 
  CheckStartupButton();
  
  SysCtlClockSet(
    SYSCTL_SYSDIV_4 |
    SYSCTL_USE_OSC |
    SYSCTL_OSC_MAIN |
    SYSCTL_XTAL_16MHZ
  );
  
  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  
  GPIOPinTypeGPIOOutput( GPIO_PORTF_BASE, GPIO_PIN_LED_RED);
  
  //Initialize UART0
  SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
  GPIOPinConfigure(GPIO_PA0_U0RX);
  GPIOPinConfigure(GPIO_PA1_U0TX);
  GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
  //0 for UART0
  //115200 bit/s
  //8 data bit - No parity - 1 Stop bit
  UARTStdioConfig( 0, 115200, SysCtlClockGet());
  UARTprintf("Started...\n");
  
  while(1) {
    UARTprintf( "%c", UARTgetc() );
  }
  
}

//Keeps waiting if the left button is pressed
void CheckStartupButton() {

  SysCtlPeripheralEnable( SYSCTL_PERIPH_GPIOF );
  GPIODirModeSet( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT, GPIO_DIR_MODE_IN );
  GPIOPadConfigSet( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT, 
                   GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU );

  while( !GPIOPinRead( GPIO_PORTF_BASE, GPIO_PIN_BTN_LEFT ) ) {}
}